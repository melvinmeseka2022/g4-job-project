resource "aws_vpc" "group4-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    name = "group4-vpc"
  }
}

resource "aws_internet_gateway" "group4-igw" {
  vpc_id = aws_vpc.group4-vpc.id

  tags = {
    Name = "group4-igw"
  }
}

resource "aws_route_table" "group4-rt" {
  vpc_id = aws_vpc.group4-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.group4-igw.id
  }

  tags = {
    Name = "group4-rt"
  }
}

resource "aws_route_table_association" "group4-rta-1" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.group4-rt.id
}

resource "aws_route_table_association" "group4-rta-2" {
  subnet_id      = aws_subnet.subnet-2.id
  route_table_id = aws_route_table.group4-rt.id
}



resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.group4-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "subnet-1"
  }
}

resource "aws_subnet" "subnet-2" {
  vpc_id            = aws_vpc.group4-vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags = {
    Name = "subnet-2"
  }
}

resource "aws_security_group" "group4-sg" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.group4-vpc.id

  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "group4-sg"
  }
}

resource "aws_instance" "group4-ec2-1" {
  ami                         = "ami-069aabeee6f53e7bf"
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.subnet-1.id
  vpc_security_group_ids      = [aws_security_group.group4-sg.id]
  key_name                    = "melvin_key"

  user_data = file("userdata1.sh")

  tags = {
    Name = "group4-ec2-1"
  }
}

resource "aws_instance" "group4-ec2-2" {
  ami                         = "ami-069aabeee6f53e7bf"
  instance_type               = "t2.micro"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.subnet-2.id
  vpc_security_group_ids      = [aws_security_group.group4-sg.id]
  key_name                    = "melvin_key"


  user_data = file("userdata2.sh")

  tags = {
    Name = "group4-ec2-2"
  }
}


# #7a Network-interface1
# resource "aws_network_interface" "webserver-nic" {
#   subnet_id       = aws_subnet.subnet-1.id
#   private_ips     = ["10.0.1.50"]
#   security_groups = [aws_security_group.group4-sg.id]
# }

# #7b Network-interface2
# resource "aws_network_interface" "webserver-nic2" {
#   subnet_id       = aws_subnet.subnet-2.id
#   private_ips     = ["10.0.2.50"]
#   security_groups = [aws_security_group.group4-sg.id]
# }