resource "aws_lb" "group4-lb" {
  name               = "group4-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.group4-lb-sg.id]
  subnets            = [aws_subnet.subnet-1.id, aws_subnet.subnet-2.id]

  #   enable_deletion_protection = true

  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.id
  #     prefix  = "test-lb"
  #     enabled = true
  #   }
}


resource "aws_security_group" "group4-lb-sg" {
  name        = "group4-lb-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.group4-vpc.id

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "group4-lb-sg"
  }
}


resource "aws_lb_listener" "group4-listener" {
  load_balancer_arn = aws_lb.group4-lb.arn
  port              = "80"
  protocol          = "HTTP"
  #   ssl_policy        = "ELBSecurityPolicy-2016-08"
  #   certificate_arn   = "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.group4-tg.arn
  }
}

resource "aws_lb_target_group" "group4-tg" {
  name     = "group4-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.group4-vpc.id

}

resource "aws_lb_target_group_attachment" "group4-tga-1" {
  target_group_arn = aws_lb_target_group.group4-tg.arn
  target_id        = aws_instance.group4-ec2-1.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "group4-tga-2" {
  target_group_arn = aws_lb_target_group.group4-tg.arn
  target_id        = aws_instance.group4-ec2-2.id
  port             = 80
}